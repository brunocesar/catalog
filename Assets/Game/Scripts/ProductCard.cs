using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ProductCard : MonoBehaviour
{
    [SerializeField] private Text _amount;
    [SerializeField] private Text _name;
    [SerializeField] private Text _price;
    [SerializeField] private Image _image;

    public void FillWithProduct(CatalogAPI.Product product)
    {
        _amount.text = product.Amount + "";
        _name.text = product.Name;
        _price.text = "$" + product.Price;
        _image.sprite = Resources.Load<Sprite>(product.Sprite);
    }
}
