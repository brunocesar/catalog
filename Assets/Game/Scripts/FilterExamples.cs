using CatalogAPI;
using UnityEngine;

public class FilterExamples : MonoBehaviour
{
    [SerializeField] private CatalogLoader _catalogLoader;

    public void FilterByItem(int itemId)
    {
        var filtered = _catalogLoader.Catalog.Buyables.Filter<Buyable>(new FilterByItem(itemId));

        foreach (Buyable buyable in filtered)
        {
            Debug.Log("Filtered: " + buyable.Name);
        }
    }

    public void SortByName()
    {
        var sorted = _catalogLoader.Catalog.Buyables.Sort(new CompareByName());

        foreach (Buyable buyable in sorted)
        {
            Debug.Log("Sorted: " + buyable.Name);
        }
    }

    public void SortByPrice()
    {
        var sorted = _catalogLoader.Catalog.Buyables.Sort(new CompareByPrice());

        foreach (Buyable buyable in sorted)
        {
            Debug.Log("Sorted: " + buyable.Price);
        }
    }

    public void SortByItemOrder()
    {
        int[] itemOrder = { 1, 3, 2 };

        var sorted = _catalogLoader.Catalog.Products.Sort(new CompareByItem(itemOrder));

        foreach (Product buyable in sorted)
        {
            Debug.Log("Sorted: " + buyable.ItemId);
        }
    }
}
