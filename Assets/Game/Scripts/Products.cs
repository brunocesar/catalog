using UnityEngine;
using CatalogAPI;
using System.Linq;
using System.Collections.Generic;
using System;

public class Products : MonoBehaviour
{

    private enum SortType
    {
        Price,
        NameLength
    };
    private enum FilterType
    {
        Default,
        Expensive
    };


    [SerializeField] private int _itemId;
    [SerializeField] private CatalogLoader _catalogLoader;
    [SerializeField] private SortType _sortType;
    [SerializeField] private FilterType _filterType;
    [SerializeField] private float _minPrice;
    [SerializeField] private ProductCard[] _cards;

    void Start()
    {
        FillCards();
    }

    private void FillCards()
    {

        var sortedProducts = _catalogLoader.Catalog.Products.Filter<Product>(ChooseFilter()).Sort<Product>(ChooseComparer()).ToList();
        
        int i = 0;
        foreach (var product in sortedProducts)
        {
            if (i >= _cards.Length)
            {
                break;
            }
            _cards[i].FillWithProduct(product);
            i++;
        }
    }

    private IComparer<Buyable> ChooseComparer()
    {
        if (_sortType == SortType.Price)
        {
            return new CompareByPrice();
        }
        else if (_sortType == SortType.NameLength)
        {
            return new CustomComparerByNameLength();
        }
        return new CompareByName();
    }

    private IFilter<Buyable> ChooseFilter()
    {
        if (_filterType == FilterType.Default)
        {
            return new FilterByItem(_itemId);
        }
        else if (_filterType == FilterType.Expensive)
        {
            return new CustomFilterByItemAndPrice(_itemId,_minPrice);
        }
        return new FilterByItem(_itemId);
    }

}



public class CustomComparerByNameLength : IComparer<Buyable>
{
    public int Compare(Buyable object1, Buyable object2)
    {
        return object1.Name.Length.CompareTo(object2.Name.Length);
    }
}


public class CustomFilterByItemAndPrice : IFilter<Buyable>
{
    int _itemId;
    float _price;
    public CustomFilterByItemAndPrice(int itemId,float price)
    {
        _itemId = itemId;
        _price = price;
    }

    public Func<Buyable, bool> GetPredicate()
    {
        return (o => o.HasItem(_itemId) && o.Price > _price);
    }
}


