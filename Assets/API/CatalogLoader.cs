namespace CatalogAPI
{
    using UnityEngine;

    public class CatalogLoader : MonoBehaviour
    {
        [SerializeField] private TextAsset _jsonFile;
        private Catalog _catalog;

        public Catalog Catalog => _catalog;

        private void Awake()
        {
            _catalog = JsonUtility.FromJson<Catalog>(_jsonFile.text);
        }
    }
}