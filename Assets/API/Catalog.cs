namespace CatalogAPI
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.Linq;

    [System.Serializable]
    public class Catalog
    {
        [SerializeField] private Item[] items;
        [SerializeField] private Product[] products;
        [SerializeField] private Bundle[] bundles;

        public IEnumerable<Item> Items => items.AsEnumerable<Item>();

        public IEnumerable<Product> Products => products.AsEnumerable<Product>();

        public IEnumerable<Bundle> Bundles => bundles.AsEnumerable<Bundle>();

        public IEnumerable<Buyable> Buyables
        {
            get
            {
                List<Buyable> buyable = new List<Buyable>();
                buyable.AddRange(products);
                buyable.AddRange(bundles);
                return buyable.AsEnumerable<Buyable>();
            }
        }

    }
}