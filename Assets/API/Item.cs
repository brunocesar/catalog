namespace CatalogAPI
{
    using UnityEngine;

    [System.Serializable]
    public class Item
    {
        [SerializeField] private int id;
        [SerializeField] private string name;

        public int Id => id;
        public string Name => name;
    }
}