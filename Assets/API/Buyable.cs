namespace CatalogAPI
{
    using UnityEngine;

    [System.Serializable]
    public abstract class Buyable
    {
        [SerializeField] private int id;
        [SerializeField] private string name;
        [SerializeField] private string description;
        [SerializeField] private float price;
        [SerializeField] private string sprite;

        public abstract bool HasItem(int itemId);

        public int Id => id;
        public string Name => name;
        public string Description => description;
        public float Price => price;
        public string Sprite => sprite;

    }
}