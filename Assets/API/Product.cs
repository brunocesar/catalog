namespace CatalogAPI
{
    using UnityEngine;

    [System.Serializable]
    public class Product : Buyable
    {
        [SerializeField] private int itemId;
        [SerializeField] private int amount;

        public int ItemId => itemId;
        public int Amount => amount;

        public override bool HasItem(int itemId)
        {
            return (itemId == this.itemId);
        }
    }
}