namespace CatalogAPI
{
    using UnityEngine;

    [System.Serializable]
    public class Bundle : Buyable
    {
        [SerializeField] private BundleItems[] items;

        public BundleItems[] Items => items;

        public override bool HasItem(int itemId)
        {
            foreach (BundleItems items in items)
            {
                if (items.ItemId == itemId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}