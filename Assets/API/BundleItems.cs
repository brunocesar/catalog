namespace CatalogAPI
{
    using UnityEngine;

    [System.Serializable]
    public class BundleItems
    {
        [SerializeField] private int itemId;
        [SerializeField] private int amount;

        public int ItemId => itemId;
        public int Amount => amount;
    }

}