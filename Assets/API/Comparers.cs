namespace CatalogAPI
{
    using System.Collections.Generic;
    using System.Linq;
    using System;

    public class CompareByItem : IComparer<Product>
    {
        int[] _itemOrder;
        public CompareByItem(int[] itemOrder)
        {
            _itemOrder = itemOrder;
        }

        public int Compare(Product object1, Product object2)
        {
            return Array.IndexOf(_itemOrder, object1.ItemId).CompareTo(Array.IndexOf(_itemOrder, object2.ItemId));
        }
    }

    public class CompareByPrice : IComparer<Buyable>
    {
        public int Compare(Buyable object1, Buyable object2)
        {
            return object1.Price.CompareTo(object2.Price);
        }
    }

    public class CompareByPriceDescending : IComparer<Buyable>
    {
        public int Compare(Buyable object1, Buyable object2)
        {
            return object2.Price.CompareTo(object1.Price);
        }
    }

    public class CompareByName : IComparer<Buyable>
    {
        public int Compare(Buyable object1, Buyable object2)
        {
            return object1.Name.CompareTo(object2.Name);
        }
    }

    public class CompareByNameDescending : IComparer<Buyable>
    {
        public int Compare(Buyable object1, Buyable object2)
        {
            return object2.Name.CompareTo(object1.Name);
        }
    }

    public static class ComparerExtensions
    {
        public static IEnumerable<T> Sort<T>(this IEnumerable<T> collection, IComparer<T> comparer)
        {
            return collection.OrderBy(x => x, comparer);
        }
    }
}