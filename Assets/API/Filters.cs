using System;
using System.Collections.Generic;
using System.Linq;

namespace CatalogAPI
{
    public interface IFilter<in T>
    {
        public Func<T, bool> GetPredicate();
    }

    public class FilterByItem : IFilter<Buyable>
    {
        int _itemId;
        public FilterByItem(int itemId)
        {
            _itemId = itemId;
        }
        public Func<Buyable, bool> GetPredicate()
        {
            return (o => o.HasItem(_itemId));
        }
    }


    public static class FilterExtensions
    {
        public static IEnumerable<T> Filter<T>(this IEnumerable<T> collection, IFilter<T> filter)
        {
            return collection.Where(filter.GetPredicate());
        }
    }

}