# Catalog API

## How To Use

- Add CatalogLoader script to a GameObject and link the json base file.
- CatalogLoader has a property called Catalog with all the catalog info.
- Catalog has Items, Products, Bundles and Buyables to access elements from catalog.
- A function called Sort can be used to sort elements. An object of type IComparer is requered for the comparision rules.
- A function called Filter can be used to filter elements. An object of type IFilter is requered for the - filter rules.
- For custom filters and sorts, its possible to extend IFilter and IComparer classes.
- Some basic examples of sorting and filtering can be found in FilterExamples script.
- A more interesting example used by games can be found on the Shop scene.

